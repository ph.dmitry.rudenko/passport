import axios from 'axios';

const main = async () => {
    try {
        const authResponse = await axios({
            method: 'POST',
            url: 'http://localhost:3000/login',
            data: {
                username: 'admin',
                password: 'password22'
            }
        })

        const { token } = authResponse.data
        console.log("token", token)

        const response = await axios({
            method: 'GET',
            url: 'http://localhost:3000/',
            headers: {
                Authorization: `Bearer ${token}`
            }
        });

        console.log("auth query", response.data)
    } catch (err: any) {
        // url
        console.log(err.response.config.url)
        // status
        console.log(err.response.status)
        // statusText
        console.log(err.response.statusText)
        // headers
        console.log(err.response.headers)
        // data
        console.log(err.response.data)
    }
}

main()