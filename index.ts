import express from 'express';
import passport from 'passport';

import morgan from 'morgan'
import bodyParser from 'body-parser';
import { createToken } from './auth';
import { init } from './base';

const app = express();

app.use(morgan('combined'));
app.use(bodyParser.json());

app.post('/login', (req: express.Request, res: express.Response) => {
    const { username, password } = req.body

    res.send({
        token: createToken(username, password)
    })
})

app.get('/',
    passport.authenticate('bearer', { session: false }),
    function (req: any, res: express.Response) {
        if (!req.user) {
            res.status(401);
            res.json({
                error: 'Unauthorized'
            })
            return;
        }

        res.json({ username: req.user.username });
    });



app.listen(3000, async () => {
    await init()

    console.log('Example app listening on port 3000!')
})