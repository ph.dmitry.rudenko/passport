import crypto from 'crypto';
import { database } from './base';
import { Strategy } from 'passport-http-bearer';
import passport from 'passport';

const SECRET_KEY = 'some_secret_key'

const hashPassword = (password: string) => {
    const unhashedToken = password + ':' + SECRET_KEY

    return crypto.createHash('sha1').update(unhashedToken).digest('hex')
}

const createToken = (username: string, password: string) => {
    const salt = (Math.floor(Math.random() * 90000) + 10000)
    const unhashedToken = username + ':' + password + ':' + salt + ':' + SECRET_KEY

    const hash = Buffer.from(unhashedToken).toString('base64')

    return hash
}

const authMiddleware = async (
    token: string,
    done: (err: Error | null, user?: any) => void
) => {
    console.log("token", token)
    const unhashedToken = Buffer.from(token, 'base64').toString('utf-8')
    const [username, password] = unhashedToken.split(':')
    console.log("unhashedToken", unhashedToken)

    let err: any = null
    const user = await database
        .select()
        .from('users')
        .where('username', username)
        .returning('*')
        .then((rows: any) => rows[0]);

    console.log("find", err, user)

    if (err) {
        console.log("DONE ERR", err)
        return done(err); 
    }
    if (!user) { 
        console.log("DONE NO USER")
        return done(null, null); 
    }

    console.log("password", password, user.password, hashPassword(password))
    if (user.password !== hashPassword(password)) {
        console.log("DONE !PASSWORD")
        return done(null, null); 
    }

    console.log("DONE SUCCESS", user)

    done(null, user);
}

const bearerStrategy = new Strategy(authMiddleware)

passport.use(bearerStrategy);


export {
    createToken,
    hashPassword
}