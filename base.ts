import knex from 'knex'
import { hashPassword } from './auth';

const database = knex({
    client: 'mysql2',
    connection: {
        host: '127.0.0.1',
        user: 'root',
        port: 3306,
        password: '00000000',
        database: 'passport'
    }
});

const init = async () => {
    const isExists = await database.schema.hasTable('users')

    if (!isExists) {
        await database.schema
            .createTable('users', table => {
                table.increments('id')
                table.string('username')
                table.string('password')
                table.string('email')
            })
    }

    // add user
    const isUserExist = await database
        .select()
        .from('users')
        .where('username', 'test')
        .then((rows: any) => rows[0])

    if (!isUserExist) {
        await database('users').insert({
            username: 'admin',
            password: hashPassword('password22'),
            email: 'admin@admin.com'
        })
    }
}

export {
    database,
    init
}